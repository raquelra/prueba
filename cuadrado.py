#la clase en mayúscula para diferenciarla de las variables en minúsculas
class Rectangulo:
    """Un ejemplo de clase para los rectangulos"""
    def __init__(self, base, altura):
        self.base = base
        self.altura = altura
    def calculo_perimetro(self):
        return self.base * 2 + self.altura * 2
    def calculo_area(self):
        return self.base * self.altura
    def __str__(self):
        return "Rectángulo de base {base} y altura {altura}: Área = {area} y Perímetro = {perimetro}".format(base=self.base, altura=self.altura, area=self.calculo_area(), perimetro=self.calculo_perimetro())
rectangulo1 = Rectangulo(5, 4)
rectangulo2 = Rectangulo(2, 1)
rectangulo3 = Rectangulo(8, 2)

print(rectangulo1)
print(rectangulo2)
print(rectangulo3)