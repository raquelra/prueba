import unittest
from fracciones import Fraccion


class TestFraccion(unittest.TestCase):
    def test_suma(self):
        f1 = Fraccion(1, 2)
        f2 = Fraccion(1, 3)
        suma = f1.suma(f2)
        self.assertNotEqual(suma, (5, 7))

unittest.main()