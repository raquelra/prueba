class Coche:
    def __init__(self, color, marca, modelo, matricula, v=0):
        self.color = color
        self.marca = marca
        self.modelo = modelo
        self.matricula = matricula
        self.velocidad = v
    def acelera(self, a):
        self.velocidad += a
        return self.velocidad
    def frena(self, a):
        self.velocidad -= a
        return self.velocidad

