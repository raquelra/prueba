class Empleado:
    def __init__(self, nombre, salario, tasa, antiguedad):
        self.__nombre = nombre
        self.__salario = salario
        self.__tasa = tasa
        self.__antiguedad = antiguedad
    def CalculoImpuestos(self):
        self.__impuestos = self.__salario * self.__tasa
        print ("El empleado {name} debe pagar {tax:.2f}".format(name=self.__nombre, tax=self.__impuestos))
        return self.__impuestos
    def ImpuestosRebajados(self):
        if self.__antiguedad > 1:
            self.__impuestos = self.__impuestos * 0.9
        print("Impuestos rebajados: " + str(self.__impuestos))
        return 
def displayCost(total):
    print("Los impuestos a pagar en total son {:.2f} euros".format(total))
emp1 = Empleado("Pepe", 20000, 0.35, 30)
emp2 = Empleado("Ana", 30000, 0.30, 27)
empleados = [emp1, emp2, Empleado("Luis", 10000, 0.10, 0.5), Empleado("Luisa", 25000, 0.15, 0.6)]
total = 0
for emp in empleados:
    total += emp.CalculoImpuestos()
    emp.ImpuestosRebajados()
displayCost(total)
