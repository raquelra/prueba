import unittest
from cuadrado import Rectangulo

class TestRectanguloCase(unittest.TestCase):
    def test_perimetro(self):
        rectangulo1 = Rectangulo(5, 5)
        perimetro1 = rectangulo1.calculo_perimetro()
        self.assertEqual(perimetro1, 20)
    def test_area(self):
        rectangulo1 = Rectangulo(5, 5)
        area1 = rectangulo1.calculo_area()
        self.assertEqual(area1, 25)

if __name__ == '__main__':
    unittest.main()