def calculo_impuestos(nombre, nom, impuesto):
    impuestos = nom * impuesto
    print ("El empleado " + nombre + " debe pagar " + str(impuestos))
    return impuestos
def display_cost(total):
    print("Los impuestos a pagar en total son " + str(total))
nomina1 = 20000
nombre1 = "Pepe"
impuesto1 = 0.2
nomina2 = 30000
nombre2 = "Ana"
impuesto2 = 0.25
nomina3 = 10000
nombre3 = "Agatha"
impuesto3 = 0.3
nomina4 = 35000
nombre4 = "Carlos"
impuesto4 = 0.4
nomina5 = 25000
nombre5 = "Eulalia"
impuesto5 = 0.45

impuestos_pepe = calculo_impuestos(nombre1, nomina1, impuesto1)
impuestos_ana = calculo_impuestos(nombre2,nomina2, impuesto2)
impuestos_agatha = calculo_impuestos(nombre3, nomina3, impuesto3)
impuestos_carlos = calculo_impuestos(nombre4, nomina4, impuesto4)
impuestos_eulalia = calculo_impuestos(nombre5, nomina5, impuesto5)
impuestos_total = impuestos_pepe + impuestos_ana + impuestos_agatha + impuestos_carlos + impuestos_eulalia
display_cost(impuestos_total)
